<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;
use Inertia\Inertia;

class DashboarUserController extends Controller
{
    public function pinjam()
    {
        return Inertia::render('Dashboard/Index', [
            'books' => Book::all()->load('book_category'),
        ]);
    }
}
