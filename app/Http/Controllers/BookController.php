<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\BookCategory;
use App\Models\Category;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Inertia::render('Book/Index', [
            'books' => Book::all()->load('book_category'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return Inertia::render('Book/Create', [
            'categories' => Category::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'penulis' => 'required',
            'penerbit' => 'required',
            'category_id' => 'required',
            'tahun_terbit' => 'required|numeric',
        ]);

        try {
            DB::beginTransaction();
            $book = Book::create($request->all());
            BookCategory::create([
                'books_id' => $book->id,
                'categories_id' => $request->category_id
            ]);
            DB::commit();
            return redirect()->route('book.index');
        } catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $book = Book::where('id', $id)->first()?->load('book_category');
        if (!$book) {
            return redirect()->back()->withErrors('Book not found!');
        }

        return Inertia::render('Book/Edit', [
            'book' => $book,
            'categories' => Category::all()
        ]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'judul' => 'required',
            'penulis' => 'required',
            'penerbit' => 'required',
            'category_id' => 'required',
            'tahun_terbit' => 'required|numeric',
        ]);



        try {
            $book = Book::where('id', $id)->first();
            if (!$book) {
                throw (new Exception('Book not found'));
            }
            $category_book = BookCategory::where('books_id', $book->id)->first();
            if (!$category_book) {
                throw (new Exception('Category Book not found'));
            }
            $category = Category::where('id', $request->category_id)->first();
            if (!$category) {
                throw (new Exception('Category not found'));
            }
            DB::beginTransaction();
            $book->update($request->all());
            $category_book->update(['categories_id' => $request->category_id]);
            DB::commit();
            return redirect()->route('book.index');
        } catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $book = Book::where('id', $id)->first();
        $book->delete();
        return redirect()->route('book.index');
    }
}
