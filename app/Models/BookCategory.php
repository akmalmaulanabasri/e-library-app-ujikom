<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookCategory extends Model
{
    use HasFactory;

    // protected $guarded = ['id'];

    protected $fillable = [
        'categories_id',
    'books_id',
    ];

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'categories_id');
    }
}
