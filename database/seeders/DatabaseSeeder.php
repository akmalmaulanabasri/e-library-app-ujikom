<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Book;
use App\Models\BookCategory;
use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $users = [
            [
                'name' => 'admin',
                'role' => 'admin',
                'username' => 'admin',
                'email' => 'admin@gmail.com',
                'address' => '-',
                'password' => bcrypt('admin'),
            ],
            [
                'name' => 'petugas',
                'role' => 'petugas',
                'username' => 'petugas',
                'email' => 'petugas@gmail.com',
                'address' => '-',
                'password' => bcrypt('petugas'),
            ],
            [
                'name' => 'user',
                'role' => 'user',
                'username' => 'user',
                'email' => 'user@gmail.com',
                'address' => '-',
                'password' => bcrypt('user'),
            ]
        ];

        $categories = [
            'Novel',
            'Science Fiction',
            'Fantasy',
            'Mystery',
            'Thriller',
            'Romance',
            'Historical Fiction',
            'Biography',
            'Self-Help',
            'Cooking',
            'Travel',
            'Business',
            'Health & Fitness',
            'Art & Photography',
            'Science',
            'History',
            'Poetry',
            'Children',
            'Young Adult',
            'Horror',
        ];

        for ($b = 0; $b < 10; $b++) {
            Book::create([
                'judul' => 'Buku dummy ' . $b,
                'penulis' => 'Penulis dummy ' . $b,
                'penerbit' => 'Penerbit dummy ' . $b,
                'tahun_terbit' => rand(2000, 2024),
            ]);

            BookCategory::create([
                'books_id' => $b + 1,
                'categories_id' => rand(1, 3),
            ]);
        }

        foreach ($users as $u) {
            User::create($u);
        }

        foreach ($categories as $cat) {
            Category::create([
                'name' => $cat
            ]);
        }

    }
}
