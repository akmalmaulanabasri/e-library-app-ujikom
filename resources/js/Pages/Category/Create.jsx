import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head, Link, useForm } from "@inertiajs/react";
import Swal from "sweetalert2";

const CreateUserForm = ({ auth }) => {
    const { data, setData, post, errors } = useForm({
        name: "",
    });

    const handleSubmit = (e) => {
        e.preventDefault();
        post(route("category.store"), {
            onSuccess: () => {
                Swal.fire("Success", "Success created category", "success");
            },
        });
    };

    return (
        <AuthenticatedLayout user={auth.user}>
            <Head title="Dashboard" />

            <div className="flex justify-center">
                <div className="mx-5 md:mx-0 md:w-1/2 p-5 my-5 bg-white overflow-hidden shadow-sm sm:rounded-lg">
                    <div className="p-3 flex justify-between items-center">
                        <span className="text-xl">Create Category</span>
                    </div>
                    <form onSubmit={handleSubmit}>
                        <div className="mb-4">
                            <label htmlFor="name" className="block mb-1">
                                Name
                            </label>
                            <input
                                id="name"
                                type="text"
                                value={data.name}
                                onChange={(e) =>
                                    setData("name", e.target.value)
                                }
                                className="w-full border border-gray-300 rounded px-3 py-2"
                            />
                            {errors.name && (
                                <div className="text-red-500">
                                    {errors.name}
                                </div>
                            )}
                        </div>

                        <button
                            type="submit"
                            className="bg-blue-500 text-white py-2 px-4 rounded hover:bg-blue-600"
                        >
                            Create Category
                        </button>
                    </form>
                </div>
            </div>
        </AuthenticatedLayout>
    );
};

export default CreateUserForm;
