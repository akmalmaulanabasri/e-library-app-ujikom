import PrimaryButton from "@/Components/PrimaryButton";
import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head, Link, router } from "@inertiajs/react";
import Swal from "sweetalert2";

export default function Index({ auth, categories }) {
    return (
        <AuthenticatedLayout user={auth.user}>
            <Head title="Category Management" />

            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                        <div className="p-3 flex justify-between items-center">
                            <span className="text-xl">Data Category</span>
                            <Link
                                href={route("category.create")}
                                className="px-5 py-2 bg-blue-400 rounded-lg"
                            >
                                Create new Category
                            </Link>
                        </div>
                        <table className="min-w-full divide-y divide-gray-200">
                            <thead className="bg-gray-50">
                                <tr>
                                    <th
                                        scope="col"
                                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                    >
                                        No
                                    </th>
                                    <th
                                        scope="col"
                                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                    >
                                        Name
                                    </th>
                                    <th
                                        scope="col"
                                        className="px-6 py-3 text-right text-xs font-medium text-gray-500 uppercase tracking-wider"
                                    >
                                        Actions
                                    </th>
                                </tr>
                            </thead>
                            <tbody className="bg-white divide-y divide-gray-200">
                                {categories.map((cat, i) => (
                                    <tr key={i}>
                                        <td className="px-6 py-4 whitespace-nowrap">
                                            <span className="text-sm text-gray-900">
                                                {cat.id}
                                            </span>
                                        </td>
                                        <td className="px-6 py-4 whitespace-nowrap">
                                            <span className="text-sm text-gray-900">
                                                {cat.name}
                                            </span>
                                        </td>
                                        <td className="px-6 py-4 whitespace-nowrap text-right">
                                            <Link
                                                href={route(
                                                    "category.edit",
                                                    cat.id
                                                )}
                                                className="px-5 py-2 bg-yellow-400 rounded-lg"
                                            >
                                                Edit Category
                                            </Link>
                                            <button
                                                onClick={() =>
                                                    router.delete(
                                                        route(
                                                            "category.destroy",
                                                            cat.id
                                                        ),
                                                        {
                                                            onSuccess: () => {
                                                                Swal.fire(
                                                                    "Success",
                                                                    "Success deleted category",
                                                                    "success"
                                                                );
                                                            },
                                                            onError: () => {
                                                                Swal.fire(
                                                                    "Error",
                                                                    "Error deleted category",
                                                                    "error"
                                                                );
                                                            },
                                                        }
                                                    )
                                                }
                                                className="px-5 py-2 ms-2 bg-red-400 rounded-lg"
                                            >
                                                Delete Category
                                            </button>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    );
}
