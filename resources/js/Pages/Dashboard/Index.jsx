import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head, Link } from "@inertiajs/react";

export default function Index({ auth, books }) {
    return (
        <AuthenticatedLayout user={auth.user}>
            <Head title="Index" />

            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="bg-white overflow-hidden shadow-sm flex-wrap flex sm:rounded-lg">
                        {books.map((b, i) => (
                            <div key={i} className="p-2 w-1/4">
                                <div className="bg-blue-200 p-2 rounded-lg">
                                    <span className="text-xl text-center flex flex-col justify-center">
                                        {b.judul}
                                    </span>
                                    <span className="text-xl text-center flex flex-col justify-center">
                                        {b.book_category.category.name}
                                    </span>
                                    <span className="text-md text-center flex flex-col justify-center">
                                        {b.penerbit} - {b.tahun_terbit}
                                    </span>
                                    <span className="text-md text-center flex flex-col justify-center">
                                        3 Dipinjam 3 Ulasan 3 Favorit
                                    </span>
                                    <div className="flex mt-3 justify-evenly">
                                        <Link
                                            href={route("book.create")}
                                            className="px-5 py-2 bg-blue-400 rounded-lg"
                                        >
                                            Pinjam
                                        </Link>
                                        <Link
                                            href={route("book.create")}
                                            className="px-5 py-2 bg-blue-400 rounded-lg"
                                        >
                                            Favorit
                                        </Link>
                                        <Link
                                            href={route("book.create")}
                                            className="px-5 py-2 bg-blue-400 rounded-lg"
                                        >
                                            Ulasan
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    );
}
