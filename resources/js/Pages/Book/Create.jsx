import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head, Link, useForm } from "@inertiajs/react";
import Swal from "sweetalert2";

const CreateUserForm = ({ auth, categories }) => {
    const { data, setData, post, errors } = useForm({
        judul: "judul",
        penulis: "penulis",
        penerbit: "penerbit",
        tahun_terbit: "2001",
        category_id: "",
    });

    const handleSubmit = (e) => {
        e.preventDefault();
        post(route("book.store"), {
            onSuccess: () => {
                Swal.fire("Success", "Success created book", "success");
            },
        });
    };

    return (
        <AuthenticatedLayout user={auth.user}>
            <Head title="Dashboard" />

            <div className="flex justify-center">
                <div className="mx-5 md:mx-0 md:w-1/2 p-5 my-5 bg-white overflow-hidden shadow-sm sm:rounded-lg">
                    <div className="p-3 flex justify-between items-center">
                        <span className="text-xl">Book Create</span>
                    </div>
                    <form onSubmit={handleSubmit}>
                        <div className="mb-4">
                            <label htmlFor="judul" className="block mb-1">
                                Judul
                            </label>
                            <input
                                id="judul"
                                type="text"
                                value={data.judul}
                                onChange={(e) =>
                                    setData("judul", e.target.value)
                                }
                                className="w-full border border-gray-300 rounded px-3 py-2"
                            />
                            {errors.judul && (
                                <div className="text-red-500">
                                    {errors.judul}
                                </div>
                            )}
                        </div>
                        <div className="mb-4">
                            <label htmlFor="penulis" className="block mb-1">
                                Penulis
                            </label>
                            <input
                                id="penulis"
                                type="text"
                                value={data.penulis}
                                onChange={(e) =>
                                    setData("penulis", e.target.value)
                                }
                                className="w-full border border-gray-300 rounded px-3 py-2"
                            />
                            {errors.penulis && (
                                <div className="text-red-500">
                                    {errors.penulis}
                                </div>
                            )}
                        </div>
                        <div className="mb-4">
                            <label htmlFor="penerbit" className="block mb-1">
                                Penerbit
                            </label>
                            <input
                                id="penerbit"
                                type="text"
                                value={data.penerbit}
                                onChange={(e) =>
                                    setData("penerbit", e.target.value)
                                }
                                className="w-full border border-gray-300 rounded px-3 py-2"
                            />
                            {errors.penerbit && (
                                <div className="text-red-500">
                                    {errors.penerbit}
                                </div>
                            )}
                        </div>
                        <div className="mb-4">
                            <label
                                htmlFor="tahun_terbit"
                                className="block mb-1"
                            >
                                Tahun Terbit
                            </label>
                            <input
                                id="tahun_terbit"
                                type="year"
                                value={data.tahun_terbit}
                                onChange={(e) =>
                                    setData("tahun_terbit", e.target.value)
                                }
                                className="w-full border border-gray-300 rounded px-3 py-2"
                            />
                            {errors.tahun_terbit && (
                                <div className="text-red-500">
                                    {errors.tahun_terbit}
                                </div>
                            )}
                        </div>

                        <div className="mb-4">
                            <label htmlFor="role" className="block mb-1">
                                Kategori
                            </label>
                            <select
                                id="role"
                                value={data.category_id}
                                onChange={(e) =>
                                    setData("category_id", e.target.value)
                                }
                                className="w-full border border-gray-300 rounded px-3 py-2"
                            >
                                <option>Select Category</option>
                                {categories.map((cat, i) => (
                                    <option value={cat.id} key={i}>
                                        {cat.name}
                                    </option>
                                ))}
                            </select>
                            {errors.category_id && (
                                <div className="text-red-500">
                                    {errors.category_id}
                                </div>
                            )}
                        </div>

                        <button
                            type="submit"
                            className="bg-blue-500 text-white py-2 px-4 rounded hover:bg-blue-600"
                        >
                            Submit
                        </button>
                    </form>
                </div>
            </div>
        </AuthenticatedLayout>
    );
};

export default CreateUserForm;
