import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head, Link, useForm } from "@inertiajs/react";
import Swal from "sweetalert2";

const CreateUserForm = ({ auth }) => {
    const { data, setData, post, errors } = useForm({
        name: "",
        username: "",
        address: "",
        email: "",
        password: "",
        role: "",
    });

    const handleSubmit = (e) => {
        e.preventDefault();
        post(route("user.store"), {
            onSuccess: () => {
                Swal.fire("Success", "Success created user", "success");
            },
        });
    };

    return (
        <AuthenticatedLayout user={auth.user}>
            <Head title="Dashboard" />

            <div className="flex justify-center">
                <div className="mx-5 md:mx-0 md:w-1/2 p-5 my-5 bg-white overflow-hidden shadow-sm sm:rounded-lg">
                    <div className="p-3 flex justify-between items-center">
                        <span className="text-xl">Data User</span>
                    </div>
                    <form onSubmit={handleSubmit}>
                        <div className="mb-4">
                            <label htmlFor="name" className="block mb-1">
                                Name
                            </label>
                            <input
                                id="name"
                                type="text"
                                value={data.name}
                                onChange={(e) =>
                                    setData("name", e.target.value)
                                }
                                className="w-full border border-gray-300 rounded px-3 py-2"
                            />
                            {errors.name && (
                                <div className="text-red-500">
                                    {errors.name}
                                </div>
                            )}
                        </div>
                        <div className="mb-4">
                            <label htmlFor="username" className="block mb-1">
                                Username
                            </label>
                            <input
                                id="username"
                                type="text"
                                value={data.username}
                                onChange={(e) =>
                                    setData("username", e.target.value)
                                }
                                className="w-full border border-gray-300 rounded px-3 py-2"
                            />
                            {errors.username && (
                                <div className="text-red-500">
                                    {errors.username}
                                </div>
                            )}
                        </div>
                        <div className="mb-4">
                            <label htmlFor="address" className="block mb-1">
                                Address
                            </label>
                            <input
                                id="address"
                                type="text"
                                value={data.address}
                                onChange={(e) =>
                                    setData("address", e.target.value)
                                }
                                className="w-full border border-gray-300 rounded px-3 py-2"
                            />
                            {errors.address && (
                                <div className="text-red-500">
                                    {errors.address}
                                </div>
                            )}
                        </div>
                        <div className="mb-4">
                            <label htmlFor="email" className="block mb-1">
                                Email
                            </label>
                            <input
                                id="email"
                                type="email"
                                value={data.email}
                                onChange={(e) =>
                                    setData("email", e.target.value)
                                }
                                className="w-full border border-gray-300 rounded px-3 py-2"
                            />
                            {errors.email && (
                                <div className="text-red-500">
                                    {errors.email}
                                </div>
                            )}
                        </div>
                        <div className="mb-4">
                            <label htmlFor="password" className="block mb-1">
                                Password
                            </label>
                            <input
                                id="password"
                                type="password"
                                value={data.password}
                                onChange={(e) =>
                                    setData("password", e.target.value)
                                }
                                className="w-full border border-gray-300 rounded px-3 py-2"
                            />
                            {errors.password && (
                                <div className="text-red-500">
                                    {errors.password}
                                </div>
                            )}
                        </div>
                        <div className="mb-4">
                            <label htmlFor="role" className="block mb-1">
                                Role
                            </label>
                            <select
                                id="role"
                                value={data.role}
                                onChange={(e) =>
                                    setData("role", e.target.value)
                                }
                                className="w-full border border-gray-300 rounded px-3 py-2"
                            >
                                <option value="">Select Role</option>
                                <option value="admin">Admin</option>
                                <option value="petugas">Petugas</option>
                                <option value="user">User</option>
                            </select>
                            {errors.role && (
                                <div className="text-red-500">
                                    {errors.role}
                                </div>
                            )}
                        </div>
                        <button
                            type="submit"
                            className="bg-blue-500 text-white py-2 px-4 rounded hover:bg-blue-600"
                        >
                            Create User
                        </button>
                    </form>
                </div>
            </div>
        </AuthenticatedLayout>
    );
};

export default CreateUserForm;
