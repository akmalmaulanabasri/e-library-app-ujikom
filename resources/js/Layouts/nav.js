const navItem = [
    {
        title: "Dashboard",
        route: "dashboard",
        active: "dashboard.*",
        roles: ["admin", "petugas", "user"],
    },
    {
        title: "User Management",
        route: "user.index",
        active: "user.*",
        roles: ["admin"],
    },
    {
        title: "Book Category",
        route: "category.index",
        active: "category.*",
        roles: ["admin"],
    },
    {
        title: "Books",
        route: "book.index",
        active: "book.*",
        roles: ["admin"],
    },
    {
        title: "Pinjam",
        route: "pinjam",
        active: "pinjam",
        roles: ["user"],
    },
];

export default navItem;
